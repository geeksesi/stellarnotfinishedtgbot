export default class Messages {

    constructor(tg) {
        this.tg = tg;
    }

    digest(d) {
        return numeral(d).format('0.0000000');
    }


    manageOffer(data, opType) {
        // const { buy, sell } = assetType(data);
        const buy = "XLM", sell = "XLM";

        if (parseInt(data.amount, 10) === 0) {
            return ("Delete offer" + sell + "for " + buy + " " + digest(data.price) + "");
        }

        if (data.offer_id === '0') {
            return ("Create offer " + this.digest(opType === 'sell' ? data.amount : data.amount * data.price) + " " + sell + " for " + digest(opType === 'sell' ? data.amount * data.price : data.amount) + " " + buy + " @ " + this.digest(data.price));
        }

        if (data.offer_id === '0') {
            return ("Update offer " + this.digest(opType === 'sell' ? data.amount : data.amount * data.price) + " " + sell + " for  " + this.digest(opType === 'sell' ? data.amount * data.price : data.amount) + " " + buy + " @" + this.digest(data.price));
        }
    }


    newTransaction(chat_id, operation_count, memo) {
        const text = "✅ New transaction \n\n Operations : " + operation_count + " \n\nMemo : " + memo + " \n\n   #transaction";
        this.tg.telegram.sendMessage(chat_id, text);
    }


    newPayment(chat_id, amount, assets, from, to) {
        const text = "✅ New payment \n\n Payment " + amount + " " + assets + " from " + from + " to " + to + " \n\n #payment";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    newOperation(chat_id, type, message) {

    }

    /**
     * TYPE : 0
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_create_account(chat_id, message) {
        const text = "Create account " + message.account + " with " + message.starting_balance + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE : 1
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_payment(chat_id, message) {
        const text = "Payment " + message.amount + " XLM from " + message.from + " to " + message.to + "";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 2
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_path_payment_strict_send(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }


    /**
     * TYPE 13
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_path_payment_strict_receive(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }


    /**
     * TYPE 3
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_manage_sell_offer(chat_id, message) {
        const text = this.manageOffer(message, 'sell');
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 12
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_manage_buy_offer(chat_id, message) {
        const text = this.manageOffer(message, 'buy');
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 4
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_create_passive_sell_offer(chat_id, message) {
        const text = " Create Passive offer " + this.digest(message.amount) + " XLM  for " + this.digest(message.amount * message.price) + " XLM @ " + this.digest(message.price) + "";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 5
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_set_options(chat_id, message) {
        if (message.low_threshold || message.med_threshold || message.high_threshold) {
            const options = [];
            if (message.low_threshold) {
                options.push(`Low: ${message.low_threshold}`);
            }

            if (message.med_threshold) {
                options.push(`Medium: ${message.med_threshold}`);
            }

            if (message.high_threshold) {
                options.push(`High: ${message.high_threshold}`);
            }

            const text = "Set options threshold [" + options.join(', ') + " ]";
            this.tg.telegram.sendMessage(chat_id, text);
        }
    }

    /**
     * TYPE 6
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_change_trust(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 7
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_allow_trust(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 8
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_account_merge(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 9
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_inflation(chat_id, message) {
        console.log("inflation")
    }

    /**
     * TYPE 10
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_manage_data(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

    /**
     * TYPE 11
     * @param {*} chat_id 
     * @param {*} message 
     */
    operation_bump_sequence(chat_id, message) {
        const text = "Path payment " + this.digest(message.amount) + " XLM to " + message.to + "  source :  " + this.digest(message.source_amount) + " XLM";
        this.tg.telegram.sendMessage(chat_id, text);
    }

}