export default class MessageHandle {
    constructor(tg) {
        this.tg = tg;
        this.new_account = [];
        this.feedback = [];
    }


    handle($) {

    }

    newAccountRequest(chat_id) {
        this.new_account.push(chat_id);
    }

    checkNewAccountRequest(chat_id) {
        return new Promise((resolve, reject) => {
            if (!this.new_account.find(e => e == chat_id)) {
                resolve(false);

            } else {
                this.new_account = this.new_account.filter(e => {
                    return e != chat_id;
                })
                resolve(true);
            }
        })
    }
}