/**
 * https://github.com/telegraf/telegraf
 */
import Telegraf, { Markup, Telegram } from 'telegraf';
import Messages from './Messages';
import MessageHandle from './MessageHandle';
/**
 * 
 */
export default class TG {

    /**
     * 
     * @param {String} TOKEN Telegram bot token 
     */
    constructor(TOKEN) {
        this.tg = new Telegraf(TOKEN);
        this.tg.start((res) => {
            console.log("WT")
            this.Keyboards(res, 'Wellcome');
        })

        this.messages = new Messages(this.tg);
        this.message_handle = new MessageHandle(this.tg);
        this.tg.on("hello", ($) => $.reply("Hi"))
        this.Events();
        this.tg.launch();
    }


    /**
     * 
     */
    Keyboards($, message) {
        return $.reply(message, Markup
            .keyboard([
                ['✳️ New Account', '👛 My Accounts'],
                ['✉️ Feedback', '⚙️ Settings'],
            ])
            .oneTime()
            .resize()
            .extra()
        )
    }

    /**
     * 
     */
    Events() {
        this.tg.hears('✳️ New Account', this.NewAccount);
        this.tg.hears('👛 My Accounts', this.MyAccounts);
        this.tg.hears('✉️ Feedback', this.Feedback);
        this.tg.hears('⚙️ Settings', this.Settings);
        this.tg.hears('⬅️ Go back', res => this.Keyboards(res, '🙋 Ok, see you later'));

        bot.on('message')
    }

    NewAccount($) {
        $.reply('Send me your Tezos address you want to monitor and the title for this address (optional). For example:\n\
\n\
        tz1XuPMB8X28jSoy7cEsXok5UVR5mfhvZLNf Arthur');
    }

    MyAccounts($) {
        $.reply("WOOPS");
    }

    Feedback($) {
        $.reply("Please, write here your message", Markup
            .keyboard([
                ['⬅️ Go back'],
            ])
            .oneTime()
            .resize()
            .extra()
        );
    }

    Settings($) {
        $.reply("WOOPS");
    }
}