import mongoose, { Schema } from 'mongoose';

export default class db {
    constructor(url) {
        mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
        this.init();
        this.account = null;
    }

    init() {
        const accountSchema = new Schema(
            {
                title: {
                    type: String,
                    required: true
                },
                address: {
                    type: String,
                    required: true
                },
                chatId: {
                    type: String,
                    required: true
                },
                operationNotification: {
                    type: Boolean,
                    required: true,
                    default: true,
                },
                transactionNotification: {
                    type: Boolean,
                    required: true,
                    default: true,
                },
                paymentNotification: {
                    type: Boolean,
                    required: true,
                    default: true,
                },
                status: {
                    type: String,
                    required: true,
                    default: 'enable',
                },
            },
            {
                timestamps: {
                    createdAt: 'created_at'
                }
            }
        );

        this.account = mongoose.model('account', accountSchema);
    }

    newAccount(chat_id, address, title = null) {
        return new Promise((resolve, reject) => {
            this.account.findOneAndUpdate({
                chatId: chat_id,
                address: address
            }, {
                chatId: chat_id,
                address: address,
                title: (title === null) ? address : title
            }, {
                new: true,
                upsert: true
            }, (err, res) => {
                if (err) {
                    reject(err);
                    return false;
                }
                resolve(res);
            })
        })
    }

    getForStream() {
        return new Promise((resolve, reject) => {
            this.account.find({ status: 'enable' }, (err, res) => {
                if (err) {
                    reject(err)
                    return false;
                }
                resolve(res);
            });
        })
    }

    toggleTransactionNotifi(chat_id, address) {
        return new Promise((resolve, reject) => {
            this.account.findOne({ chatId: chat_id, address: address }, (err, account) => {
                if (err) {
                    reject(err);
                    return false;
                }

                account.transactionNotification = !account.transactionNotification;
                account.save((save_err, save_res) => {
                    if (save_err) {
                        reject(save_err);
                        return false;
                    }
                    resolve(save_res);
                });
            });
        })
    }


    toggleOperationNotifi(chat_id, address) {
        return new Promise((resolve, reject) => {
            this.account.findOne({ chatId: chat_id, address: address }, (err, account) => {
                if (err) {
                    reject(err);
                    return false;
                }

                account.operationNotification = !account.operationNotification;
                account.save((save_err, save_res) => {
                    if (save_err) {
                        reject(save_err);
                        return false;
                    }
                    resolve(save_res);
                });
            });
        })
    }


    togglePaymentNotifi(chat_id, address) {
        return new Promise((resolve, reject) => {
            this.account.findOne({ chatId: chat_id, address: address }, (err, account) => {
                if (err) {
                    reject(err);
                    return false;
                }

                account.paymentNotification = !account.paymentNotification;
                account.save((save_err, save_res) => {
                    if (save_err) {
                        reject(save_err);
                        return false;
                    }
                    resolve(save_res);
                });
            });
        });
    }

}