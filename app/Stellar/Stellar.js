
import StellarSdk from 'stellar-sdk';

export default class Stellar {
    constructor(SERVER_ADDRESS, tg) {
        this.server = new StellarSdk.Server(SERVER_ADDRESS);
        this.tg = tg
        /**
         * {
         *  chatID : {
         *      address:{
         *          operationNotify : object
         *          transactionNotify : object
         *          paymentNotify : object
         *      }
         *  }
         * }
         */
        this.sessions = {}
    }

    start_sessions() {

    }

    checkAddressValidation(address, cb) {
        if (typeof address !== 'string' || address.length !== 56) {
            cb(false);
            return false;
        }
        this.server.loadAccount(address).then(res => {
            cb(res);
        }).catch(err => {
            cb(false);
        });
    }

    newHandler(chat_id, address) {
        const tmp = {};
        tmp[address] = {
            operationNotify: this.server.operations()
                .forAccount(address)
                .cursor('now')
                .stream({
                    onmessage: message => {
                        this.operationHandle(chat_id, message);
                    }
                }),
            paymentNotify: this.server.payments()
                .forAccount(address)
                .cursor('now')
                .stream({
                    onmessage: message => {
                        // this.paymentHandle(chat_id, message);
                    }
                }),
            transactionNotify: this.server.transactions()
                .forAccount(address)
                .cursor('now')
                .stream({
                    onmessage: message => {
                        // this.transactionHandle(chat_id, message);
                    }
                }),
        }
        this.sessions[chat_id] = tmp;
    }


    transactionHandle(chat_id, message) {
        const number_of_operations = message.operation_count;
        const memo = (message.memo_type == 'none') ? 'None' : message.memo;
        this.tg.messages.newTransaction(chat_id, number_of_operations, memo);
    }


    paymentHandle(chat_id, message) {
        const amount = message.amount;
        const asset = message.asset_type;
        const from_address = message.from;
        const to_address = message.to;

        this.tg.messages.newPayment(chat_id, amount, asset, from_address, to_address);
    }

    operationHandle(chat_id, message) {
        // console.log(message);
    }
}