import TG from './TG/TG';
import Stellar from './Stellar/Stellar';

const TOKEN = process.env.TOKEN;
const STELLAR_SERVER = process.env.STELLAR_SERVER || 'https://horizon.stellar.org';

const tg = new TG(TOKEN);
const stellar = new Stellar(STELLAR_SERVER, tg);

// stellar.checkAddressValidation('GCM4PT6XDZBWOOENDS6FOU22GJQLJPV2GC7VRVII4TFGZBA3ZXNM55SV', (res) => {
//     console.log(res);
// });

stellar.newHandler("91416644", "GCM4PT6XDZBWOOENDS6FOU22GJQLJPV2GC7VRVII4TFGZBA3ZXNM55SV");